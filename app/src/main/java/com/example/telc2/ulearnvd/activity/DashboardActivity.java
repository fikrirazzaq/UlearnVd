package com.example.telc2.ulearnvd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.fragment.ExploreFragment;
import com.example.telc2.ulearnvd.fragment.HomeFragment;
import com.example.telc2.ulearnvd.fragment.MyCourseFragment;
import com.example.telc2.ulearnvd.fragment.NotificationsFragment;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DashboardActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {

    public Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MaterialSearchView searchView;
    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        toolbar = (Toolbar) findViewById(R.id.toolbar_dashboard);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager_dashboard);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs_dashboard);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setContentDescription(toolbar.getTitle().toString());
        setupTabIcons();

        tts = new TextToSpeech(this, this);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()) {
                    case 0:
                        viewPager.setCurrentItem(0);
                        toolbar.setTitle("Ulearning - Beranda");
                        break;
                    case 1:
                        viewPager.setCurrentItem(1);
                        toolbar.setTitle("Ulearning - Eksplorasi Pelajaran");
                        break;
                    case 2:
                        viewPager.setCurrentItem(2);
                        toolbar.setTitle("Ulearning - Pelajaran Saya");
                        break;
                    case 3:
                        viewPager.setCurrentItem(3);
                        toolbar.setTitle("Ulearning - Notifikasi");
                        break;
                    default:
                        viewPager.setCurrentItem(tab.getPosition());
                        toolbar.setTitle("Ulearning");
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setVoiceSearch(true);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
        searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Snackbar.make(findViewById(R.id.container), "Query: " + query, Snackbar.LENGTH_LONG)
                        .show();

                if (query.toLowerCase().equals("notifikasi")) {
                    tts.speak(query, TextToSpeech.QUEUE_FLUSH, null);
                    viewPager.setCurrentItem(3);
                } else if (query.toLowerCase().equals("buka pelajaran saya")) {
                    tts.speak(query, TextToSpeech.QUEUE_FLUSH, null);
                    viewPager.setCurrentItem(2);
                } else if (query.toLowerCase().equals("buka pelajaran terakhir")) {
                    tts.speak(query, TextToSpeech.QUEUE_FLUSH, null);
                    Intent intent = new Intent(DashboardActivity.this, TabbedLessonActivity.class);
                    startActivity(intent);
                } else if (query.toLowerCase().equals("buka kuis terakhir")) {
                    tts.speak(query, TextToSpeech.QUEUE_FLUSH, null);
                    Intent intent = new Intent(DashboardActivity.this, TabbedKuisActivity.class);
                    startActivity(intent);
                } else if (query.toLowerCase().equals("bantuan")) {
                    tts.speak("Bantuan yang tersedia adalah, buka notifikasi, buka pelajaran saya, buka pelajaran terakhir, dan buka kuis terakhir", TextToSpeech.QUEUE_FLUSH, null);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                searchView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSearchViewClosed() {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tab, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    private void setupTabIcons() {
        int[] tabIcons = {
                R.drawable.ic_home,
                R.drawable.ic_explore,
                R.drawable.ic_bookmark_border,
                R.drawable.ic_notifications
        };

        tabLayout.getTabAt(0).setIcon(tabIcons[0]).setContentDescription("Beranda");
        tabLayout.getTabAt(1).setIcon(tabIcons[1]).setContentDescription("Eksplorasi Pelajaran");
        tabLayout.getTabAt(2).setIcon(tabIcons[2]).setContentDescription("Pelajaran Saya");
        tabLayout.getTabAt(3).setIcon(tabIcons[3]).setContentDescription("Notifikasi");
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeFragment(), "Home");
        adapter.addFrag(new ExploreFragment(), "Explore");
        adapter.addFrag(new MyCourseFragment(), "My Course");
        adapter.addFrag(new NotificationsFragment(), "Notifications");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(new Locale("id","ID"));

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
//                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches != null && matches.size() > 0) {
                String searchWrd = matches.get(0);
                if (!TextUtils.isEmpty(searchWrd)) {
                    searchView.setQuery(searchWrd, false);
                }
            }

            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        // Don't forget to shutdown tts!
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

//    @Override
//    public void onBackPressed() {
//        if(getFragmentManager().getBackStackEntryCount() > 0)
//            getFragmentManager().popBackStack();
//        else
//            super.onBackPressed();
//    }
}
