package com.example.telc2.ulearnvd.fragment;


import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.telc2.ulearnvd.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Video2WizardFragment extends Fragment {

    private TextView txJudul, txIsi, txIsi2;
    private VideoView videoLesson;
    private LinearLayout linearVideo, linearTeori, linearAssignment;
    private Button btnTeori1, btnTeori2, btnVideo1, btnVideo2, btnAssignment;
    private Button btnSebelumnya, btnSelanjutnya;
    private TextView halaman;
    ProgressDialog pDialog;
    String VideoURL = "http://juvetic.telclab.com/uelarn/pertumbuhan%20dan%20perkembangan%20tumbuhan.mp4";

    public Video2WizardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_video2_wizard, container, false);

        videoLesson = (VideoView) v.findViewById(R.id.video_activity_wizard_lesson_video2);
        linearVideo = (LinearLayout) v.findViewById(R.id.linearvideo_video2);

        linearAssignment = (LinearLayout) v.findViewById(R.id.linear_item_isi_lesson_video2);
        linearTeori = (LinearLayout) v.findViewById(R.id.linear_item_isi_lesson2_video2);

        btnTeori1 = (Button) v.findViewById(R.id.tx_item_isi_lesson_video2);
        btnTeori2 = (Button) v.findViewById(R.id.tx_item_isi_lesson_video2a);
        btnVideo1 = (Button) v.findViewById(R.id.tx_item_isi_lesson2);
        btnVideo2 = (Button) v.findViewById(R.id.tx_item_isi_lesson2a);

        final ViewPager vp = (ViewPager) getActivity().findViewById(R.id.container2);

        btnSebelumnya = (Button) v.findViewById(R.id.btn_teori_sebelumnya_video2);
        btnSelanjutnya = (Button) v.findViewById(R.id.btn_teori_selanjutnya_video2);
        btnSelanjutnya.setVisibility(View.INVISIBLE);


        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    getContext());
            mediacontroller.setAnchorView(videoLesson);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(VideoURL);
            videoLesson.setMediaController(mediacontroller);
            videoLesson.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoLesson.requestFocus();
        videoLesson.start();
        videoLesson.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                videoLesson.start();
            }
        });

        btnTeori1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(0);
                videoLesson.pause();
            }
        });

        btnTeori2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(1);
                videoLesson.pause();
            }
        });

        btnVideo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(2);
                videoLesson.pause();
            }
        });

        btnVideo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(3);
            }
        });

        btnSebelumnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() - 1);
                videoLesson.pause();
            }
        });

        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() + 1);
                videoLesson.pause();
            }
        });

        return v;
    }

}
