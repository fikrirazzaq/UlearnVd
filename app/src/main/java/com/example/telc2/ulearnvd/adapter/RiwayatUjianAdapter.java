package com.example.telc2.ulearnvd.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.activity.HasilKuisActivity;
import com.example.telc2.ulearnvd.data.model.RiwayatPembelajaran;
import com.example.telc2.ulearnvd.data.model.RiwayatUjian;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.List;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class RiwayatUjianAdapter extends RecyclerView.Adapter<RiwayatUjianAdapter.HolderData> {

    private List<RiwayatUjian> riwayatUjianList;
    private ItemClickListener clickListener;
    private Context mContext;

    public RiwayatUjianAdapter(List<RiwayatUjian> riwayatUjianList, Context context) {
        this.riwayatUjianList = riwayatUjianList;
        mContext = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragment_home_riwayat_ujian, parent, false);
        return new RiwayatUjianAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        RiwayatUjian riwayatUjian = riwayatUjianList.get(position);
        holder.txNamaUjian.setText(riwayatUjian.getNamaUjian());
        holder.txNamaCourse.setText(riwayatUjian.getNamaCourse());
        holder.txNilaiUjian.setText(riwayatUjian.getNilaiUjian());
    }

    @Override
    public int getItemCount() {
        return riwayatUjianList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txNamaUjian, txNamaCourse, txNilaiUjian;

        public HolderData(View itemView) {
            super(itemView);
            txNamaCourse = (TextView) itemView.findViewById(R.id.tx_home_nama_course);
            txNamaUjian = (TextView) itemView.findViewById(R.id.tx_home_nama_ujian);
            txNilaiUjian = (TextView) itemView.findViewById(R.id.tx_home_nilai_ujian);

            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }
}
