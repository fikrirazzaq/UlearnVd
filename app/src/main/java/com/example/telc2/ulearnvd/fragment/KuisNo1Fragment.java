package com.example.telc2.ulearnvd.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.telc2.ulearnvd.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class KuisNo1Fragment extends Fragment {

    private Button btnSebelumnya, btnSelanjutnya;

    public KuisNo1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_kuis_no1, container, false);

        btnSebelumnya = (Button) v.findViewById(R.id.btn_teori_sebelumnya_no1);
        btnSelanjutnya = (Button) v.findViewById(R.id.btn_teori_selanjutnya_no1);

        btnSebelumnya.setVisibility(View.INVISIBLE);

        final ViewPager vp = (ViewPager) getActivity().findViewById(R.id.container3);

        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() + 1);
            }
        });

        return v;
    }

}
