package com.example.telc2.ulearnvd.wizard.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.telc2.ulearnvd.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class KuisMulaiFragment extends Fragment {


    public KuisMulaiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_kuis_mulai, container, false);
    }

}
