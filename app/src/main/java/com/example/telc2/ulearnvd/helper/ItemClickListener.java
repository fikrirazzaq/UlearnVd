package com.example.telc2.ulearnvd.helper;

import android.view.View;

/**
 * Created by TEL-C on 8/9/17.
 */

public interface ItemClickListener {
    public void onClick(View view, int position);
}
