package com.example.telc2.ulearnvd.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.adapter.NotifAdapter;
import com.example.telc2.ulearnvd.data.model.Notif;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment {

    private List<Notif> notifList = new ArrayList<>();
    private RecyclerView recyclerView;
    private NotifAdapter mAdapter;

    public NotificationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rcv_notif);

        mAdapter = new NotifAdapter(notifList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        notifList.clear();

        dummyNotifData();

        return view;
    }

    private void dummyNotifData() {
        Notif notif = new Notif("Reza", "Biologi Kelas 12", "1 jam yang lalu");
        notifList.add(notif);

        mAdapter.notifyDataSetChanged();
    }
}
