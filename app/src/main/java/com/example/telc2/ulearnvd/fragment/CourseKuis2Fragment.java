package com.example.telc2.ulearnvd.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.activity.TabbedKuisActivity;
import com.example.telc2.ulearnvd.adapter.CourseKuisAdapter;
import com.example.telc2.ulearnvd.data.model.CourseKuis;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CourseKuis2Fragment extends Fragment implements ItemClickListener {

    private List<CourseKuis> courseKuisList = new ArrayList<>();
    private CourseKuisAdapter courseKuisAdapter;
    private RecyclerView rcvCourseKuis;

    public CourseKuis2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_course_kuis2, container, false);

        //--Pair View
//        rcvCourseKuis = (RecyclerView) v.findViewById(R.id.rcv_fragment_course_kuis);

        //--Adapter Riwayat Pembelajaran
//        courseKuisAdapter = new CourseKuisAdapter(courseKuisList);
//        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(getContext());
//        rcvCourseKuis.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
//        rcvCourseKuis.setLayoutManager(mLayoutManagerCourse);
//        rcvCourseKuis.setItemAnimator(new DefaultItemAnimator());
//        rcvCourseKuis.setAdapter(courseKuisAdapter);
//        courseKuisAdapter.setClickListener(this);


        //--Populate Data Course Kuis
        courseKuisList.clear();
//        dummyCourseKuisData();

        return v;
    }

    public void dummyCourseKuisData() {
        CourseKuis courseKuis;

        courseKuis = new CourseKuis("Quis 1", "BAB 1: Pertumbuhan dan Perkembangan", "5 Pertanyaan", "15 Menit", "-", "-");
        courseKuisList.add(courseKuis);

        courseKuisAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view, int position) {
        Intent intent = new Intent(getContext(), TabbedKuisActivity.class);
        startActivity(intent);
    }
}