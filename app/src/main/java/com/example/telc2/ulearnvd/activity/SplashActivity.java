package com.example.telc2.ulearnvd.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.telc2.ulearnvd.R;

public class SplashActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        sharedPreferences = getSharedPreferences("ShaPreferences", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        boolean  firstTime = sharedPreferences.getBoolean("first", true);
//        if(firstTime) {
//            editor.putBoolean("first",false);
//            //For commit the changes, Use either editor.commit(); or  editor.apply();.
//            editor.apply();
//            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//            startActivity(intent);
//        }
//        else
//        {
//            Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);
//            startActivity(intent);
//        }
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
