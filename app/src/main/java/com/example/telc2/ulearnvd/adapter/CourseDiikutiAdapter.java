package com.example.telc2.ulearnvd.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.CourseDiikuti;
import com.example.telc2.ulearnvd.helper.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class CourseDiikutiAdapter extends RecyclerView.Adapter<CourseDiikutiAdapter.HolderData> {

    private List<CourseDiikuti> courseDiikutiList;
    private ItemClickListener clickListener;
    private Context mContext;

    public CourseDiikutiAdapter(List<CourseDiikuti> courseDiikutiList, Context context) {
        this.courseDiikutiList = courseDiikutiList;
        mContext = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_fragment_my_course_course_diikuti, parent, false);
        return new CourseDiikutiAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        CourseDiikuti courseDiikuti = courseDiikutiList.get(position);
        holder.txNamaCourse.setText(courseDiikuti.getNamaCourse());
        holder.txKelasCourse.setText(courseDiikuti.getKelasCourse());
        holder.txGuru.setText(courseDiikuti.getGuruCourse());
        Picasso.with(mContext).load(courseDiikutiList.get(position).getGambarCourse()).into(holder.imgCourse);

    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return courseDiikutiList.size();
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txNamaCourse, txKelasCourse, txGuru;
        private ImageView imgCourse;

        public HolderData(View itemView) {
            super(itemView);

            txNamaCourse = (TextView) itemView.findViewById(R.id.tx_mycourse_nama_course);
            txKelasCourse = (TextView) itemView.findViewById(R.id.tx_mycourse_kelas);
            txGuru = (TextView) itemView.findViewById(R.id.tx_mycourse_nama_guru);
            imgCourse = (ImageView) itemView.findViewById(R.id.img_mycourse_gambar);

            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }
}
