package com.example.telc2.ulearnvd.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.CourseDiikuti;
import com.example.telc2.ulearnvd.data.model.LessonBab;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by TEL-C on 8/7/17.
 */

public class LessonBabAdapter extends RecyclerView.Adapter<LessonBabAdapter.HolderData> {

    private List<LessonBab> lessonBabList;
    private Context mContext;

    public LessonBabAdapter(List<LessonBab> lessonBabList, Context context) {
        this.lessonBabList = lessonBabList;
        context = mContext;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activity_detil_course_lessonbab, parent, false);
        return new LessonBabAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        LessonBab lessonBab = lessonBabList.get(position);
        holder.txNomorLesson.setText(lessonBab.getNomorLesson());
        holder.txNamaLesson.setText(lessonBab.getNamaLesson());

        Picasso.with(mContext).load(lessonBabList.get(position).getGambarLesson()).into(holder.imgLesson);
    }

    @Override
    public int getItemCount() {
        return lessonBabList.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {

        private TextView txNomorLesson, txNamaLesson;
        private ImageView imgLesson;

        public HolderData(View itemView) {
            super(itemView);

            txNomorLesson = (TextView) itemView.findViewById(R.id.tx_item_detil_course_nomorlesson);
            txNamaLesson = (TextView) itemView.findViewById(R.id.tx_item_detil_course_namalesson);
            imgLesson = (ImageView) itemView.findViewById(R.id.img_item_detil_course_lessonbab);
        }
    }
}
