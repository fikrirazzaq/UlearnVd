package com.example.telc2.ulearnvd.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class App extends Application {

    private static Context context;

    @Override
    public void onCreate()
    {
        super.onCreate();
        context = this.getApplicationContext();
    }

    public static Context getContext(){
        return context;
    }
}