package com.example.telc2.ulearnvd.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.CourseHomeLessonBab;
import com.example.telc2.ulearnvd.data.model.LessonBab;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.List;

/**
 * Created by TEL-C on 8/8/17.
 */

public class CourseHomeLessonBabAdapter extends RecyclerView.Adapter<CourseHomeLessonBabAdapter.HolderData> {

    private List<CourseHomeLessonBab> courseHomeLessonBabList;
    private ItemClickListener clickListener;

    public CourseHomeLessonBabAdapter(List<CourseHomeLessonBab> courseHomeLessonBabList) {
        this.courseHomeLessonBabList = courseHomeLessonBabList;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activity_detil_course_lessonbab, parent, false);
        return new CourseHomeLessonBabAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        CourseHomeLessonBab courseHomeLessonBab = courseHomeLessonBabList.get(position);
        holder.txNomorLesson.setText(courseHomeLessonBab.getNomorLesson());
        holder.txNamaLesson.setText(courseHomeLessonBab.getNamaLesson());
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return courseHomeLessonBabList.size();
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txNomorLesson, txNamaLesson;

        public HolderData(View itemView) {
            super(itemView);

            txNomorLesson = (TextView) itemView.findViewById(R.id.tx_item_detil_course_nomorlesson);
            txNamaLesson = (TextView) itemView.findViewById(R.id.tx_item_detil_course_namalesson);

            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }
}