package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TELC2 on 8/6/2017.
 */

public class RiwayatUjian {

    private String namaUjian, namaCourse, gambarCourse, nilaiUjian;

    public RiwayatUjian(String namaUjian, String namaCourse, String gambarCourse, String nilaiUjian) {
        this.namaUjian = namaUjian;
        this.namaCourse = namaCourse;
        this.gambarCourse = gambarCourse;
        this.nilaiUjian = nilaiUjian;
    }

    public RiwayatUjian(String namaUjian, String namaCourse, String nilaiUjian) {
        this.namaUjian = namaUjian;
        this.namaCourse = namaCourse;
        this.nilaiUjian = nilaiUjian;
    }

    public String getNilaiUjian() {
        return nilaiUjian;
    }

    public void setNilaiUjian(String nilaiUjian) {
        this.nilaiUjian = nilaiUjian;
    }

    public String getNamaUjian() {
        return namaUjian;
    }

    public void setNamaUjian(String namaUjian) {
        this.namaUjian = namaUjian;
    }

    public String getNamaCourse() {
        return namaCourse;
    }

    public void setNamaCourse(String namaCourse) {
        this.namaCourse = namaCourse;
    }

    public String getGambarCourse() {
        return gambarCourse;
    }

    public void setGambarCourse(String gambarCourse) {
        this.gambarCourse = gambarCourse;
    }
}
