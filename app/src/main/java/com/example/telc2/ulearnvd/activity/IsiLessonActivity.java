package com.example.telc2.ulearnvd.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.adapter.IsiKategoriCourseAdapter;
import com.example.telc2.ulearnvd.adapter.SubLessonAdapter;
import com.example.telc2.ulearnvd.data.model.IsiKategoriCourse;
import com.example.telc2.ulearnvd.data.model.SubLesson;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;
import com.example.telc2.ulearnvd.helper.ItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class IsiLessonActivity extends AppCompatActivity implements View.OnClickListener {

    private List<SubLesson> subLessonList = new ArrayList<>();
    private SubLessonAdapter subLessonAdapter;
    private RecyclerView rcvSubLesson;
    private TextView txIsiLessonJudul, txIsiLessonDeskp;
    private Button btnTeori, btnKuis, btnVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biologi4);

        setTitle("IPA Kelas 12");

        //--Pair View
//        rcvSubLesson = (RecyclerView) findViewById(R.id.rcv_biologi4_sublesson);
        txIsiLessonJudul = (TextView) findViewById(R.id.tx_biologi4_judul);
        txIsiLessonDeskp = (TextView) findViewById(R.id.tx_biologi4_deskp);
        btnTeori = (Button) findViewById(R.id.tx_item_biologi4);
        btnVideo = (Button) findViewById(R.id.tx_item_biologi42);
        //--Adapter Riwayat Pembelajaran
//        subLessonAdapter = new SubLessonAdapter(subLessonList);
//        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(this);
//        rcvSubLesson.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
//        rcvSubLesson.setLayoutManager(mLayoutManagerCourse);
//        rcvSubLesson.setItemAnimator(new DefaultItemAnimator());
//        rcvSubLesson.setAdapter(subLessonAdapter);
//        subLessonAdapter.setClickListener(this);

//        rcvIsiKategoriCourse.setContentDescription(txCourseName.getText().toString());

        btnTeori.setOnClickListener(this);
        btnVideo.setOnClickListener(this);

        //--Back Button Action Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //--Isi judul dan deskripsi Lesson
        txIsiLessonJudul.setText("BAB 1: Pertumbuhan dan Perkembangan");
        txIsiLessonDeskp.setText("Selamat datang di pelajaran Biologi Kelas 12 kita akan mempelajari Biologi dari berbagai sisi, dari sel individu hingga organisme ke seluruh biosfer (planet bumi). Langsung masuk untuk mempelajari lebih lanjut tentang tema utama biologi dan sifat makhluk.");

        //--Populate Data Lesson Bab
        subLessonList.clear();
//        dummyIsiLessonData();
    }

    public void dummyIsiLessonData() {
        SubLesson subLesson;


        subLesson = new SubLesson("Teori Pertumbuhan, Perkembangan, dan Pemersiban");
        subLessonList.add(subLesson);


        subLessonAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnTeori) {
            Intent intent = new Intent(IsiLessonActivity.this, WizardLessonActivity.class);
            startActivity(intent);
        }
    }
}
