package com.example.telc2.ulearnvd.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.data.model.IsiKategoriCourse;
import com.example.telc2.ulearnvd.data.model.KategoriCourse;
import com.example.telc2.ulearnvd.helper.ItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by TEL-C on 8/9/17.
 */

public class KategoriCourseAdapter extends RecyclerView.Adapter<KategoriCourseAdapter.HolderData> {

    private List<KategoriCourse> kategoriCourseList;
    private ItemClickListener clickListener;
    private Context mContext;

    public KategoriCourseAdapter(List<KategoriCourse> kategoriCourseList, Context context) {
        this.kategoriCourseList = kategoriCourseList;
        this.mContext = context;
    }

    @Override
    public HolderData onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_activity_kategori_course, parent, false);
        return new KategoriCourseAdapter.HolderData(itemView);
    }

    @Override
    public void onBindViewHolder(HolderData holder, int position) {
        KategoriCourse kategoriCourse = kategoriCourseList.get(position);
        holder.txNamaKategori.setText(kategoriCourse.getNamaKategori());
        Picasso.with(mContext).load(kategoriCourseList.get(position).getGambarKategori()).into(holder.imgKategori);
    }

    @Override
    public int getItemCount() {
        return kategoriCourseList.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public class HolderData extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView txNamaKategori;
        private ImageView imgKategori;

        public HolderData(View itemView) {
            super(itemView);

            txNamaKategori = (TextView) itemView.findViewById(R.id.tx_kategori_course_nama);
            imgKategori = (ImageView) itemView.findViewById(R.id.img_kategori_course_gambar);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
        }
    }

}
