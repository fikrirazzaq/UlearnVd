package com.example.telc2.ulearnvd.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.adapter.CourseDiikutiAdapter;
import com.example.telc2.ulearnvd.adapter.LessonBabAdapter;
import com.example.telc2.ulearnvd.data.model.CourseDiikuti;
import com.example.telc2.ulearnvd.data.model.LessonBab;
import com.example.telc2.ulearnvd.helper.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class DetilCourseActivity extends AppCompatActivity  implements View.OnClickListener {

    private List<LessonBab> lessonBabList = new ArrayList<>();
    private LessonBabAdapter lessonBabAdapter;
    private RecyclerView rcvLessonBab;
    private Button btnMendaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_course);
        setTitle("");

        //--Pair View
        rcvLessonBab = (RecyclerView) findViewById(R.id.rcv_detil_course_lessonbab);
        btnMendaftar = (Button) findViewById(R.id.btn_detil_course_mendaftar);

        //--Adapter Riwayat Pembelajaran
        lessonBabAdapter = new LessonBabAdapter(lessonBabList, getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerCourse = new LinearLayoutManager(this);
        rcvLessonBab.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rcvLessonBab.setLayoutManager(mLayoutManagerCourse);
        rcvLessonBab.setItemAnimator(new DefaultItemAnimator());
        rcvLessonBab.setAdapter(lessonBabAdapter);

        //--Elevation=0 dan Back Button Action Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);


        //--Enable Click Handling
        btnMendaftar.setOnClickListener(this);

        //--Populate Data Lesson Bab
//        dummyLessonBabData();
    }

    public void dummyLessonBabData() {
        LessonBab lessonBab;


        lessonBab = new LessonBab("http://juvetic.telclab.com/uelarn/images/Leson%20Ipa.png", "BAB 1", "Pertumbuhan, dan Perkembangan pada Tumbuhan");
        lessonBabList.add(lessonBab);

        lessonBab = new LessonBab("http://juvetic.telclab.com/uelarn/images/Leson%20Ipa.png", "BAB 2", "Proses Metabolisme Organisme");
        lessonBabList.add(lessonBab);

        lessonBab = new LessonBab("http://juvetic.telclab.com/uelarn/images/Leson%20Ipa.png", "BAB 3", "Genetika");
        lessonBabList.add(lessonBab);

        lessonBab = new LessonBab("http://juvetic.telclab.com/uelarn/images/Leson%20Ipa.png", "BAB 4", "Pola-Pola Hereditas");
        lessonBabList.add(lessonBab);

        lessonBabAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btnMendaftar) {
            final Dialog dialog = new Dialog(this) {
                @Override
                public boolean onTouchEvent(MotionEvent event) {
                    this.dismiss();
                    finish();
                    Intent intent = new Intent(getContext(), CompleteCourseActivity.class);
                    startActivity(intent);
                    return true;
                }
            };

            dialog.setContentView(R.layout.alert_daftar_course);
            dialog.show();
        }
    }
}