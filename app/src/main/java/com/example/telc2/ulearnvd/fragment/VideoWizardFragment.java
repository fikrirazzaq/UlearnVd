package com.example.telc2.ulearnvd.fragment;


import android.app.ProgressDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.activity.WizardLessonActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoWizardFragment extends Fragment {

    private TextView txJudul, txIsi, txIsi2;
    private VideoView videoLesson;
    private LinearLayout linearVideo, linearTeori, linearAssignment;
    private Button btnTeori1, btnTeori2, btnVideo1, btnVideo2;
    private Button btnSebelumnya, btnSelanjutnya;
    private TextView halaman;
    ProgressDialog pDialog;
    String VideoURL = "http://juvetic.telclab.com/uelarn/Pertumbuhan%20dan%20Perkembangan%20pada%20Tumbuhan%20part%201%20-%20YouTube_h263.3gp";

    public VideoWizardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_video_wizard, container, false);

        videoLesson = (VideoView) v.findViewById(R.id.video_activity_wizard_lesson_video1);
        linearVideo = (LinearLayout) v.findViewById(R.id.linearvideo_video1);

        linearAssignment = (LinearLayout) v.findViewById(R.id.linear_item_isi_lesson_video1);

        btnTeori1 = (Button) v.findViewById(R.id.tx_item_isi_lesson_video1);
        btnTeori2 = (Button) v.findViewById(R.id.tx_item_isi_lesson_video1a);
        btnVideo1 = (Button) v.findViewById(R.id.tx_item_isi_lesson2);

        final ViewPager vp = (ViewPager) getActivity().findViewById(R.id.container2);

        btnSebelumnya = (Button) v.findViewById(R.id.btn_teori_sebelumnya_video1);
        btnSelanjutnya = (Button) v.findViewById(R.id.btn_teori_selanjutnya_video1);

        btnTeori1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(0);
                videoLesson.pause();
            }
        });

        btnTeori2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(1);
                videoLesson.pause();
            }
        });

        btnVideo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(2);
            }
        });


        btnSebelumnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() - 1);
            }
        });

        btnSelanjutnya.setVisibility(View.INVISIBLE);
        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() + 1);
                videoLesson.pause();
            }
        });

        try {
            // Start the MediaController
            MediaController mediacontroller = new MediaController(
                    getContext());
            mediacontroller.setAnchorView(videoLesson);
            // Get the URL from String VideoURL
            Uri video = Uri.parse(VideoURL);
            videoLesson.setMediaController(mediacontroller);
            videoLesson.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoLesson.requestFocus();
        videoLesson.start();
        videoLesson.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                videoLesson.pause();
            }
        });


        return v;
    }

}
