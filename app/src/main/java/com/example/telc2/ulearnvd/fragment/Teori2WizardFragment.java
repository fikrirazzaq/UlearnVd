package com.example.telc2.ulearnvd.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.telc2.ulearnvd.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Teori2WizardFragment extends Fragment {

    private TextView txJudul, txIsi, txIsi2;
    private VideoView videoLesson;
    private LinearLayout linearVideo, linearTeori, linearAssignment;
    private Button btnTeori, btnVideo, btnAssignment;
    private Button btnSebelumnya, btnSelanjutnya;
    private TextView halaman;
    ProgressDialog pDialog;
    String VideoURL = "http://juvetic.telclab.com/pertumbuhan%20dan%20perkembangan%20tumbuhan.mp4";

    public Teori2WizardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_teori2_wizard, container, false);

        String isi = "<p><strong>Apa sih yang dimaksud dengan pertumbuhan dan perkembangan ?</strong></p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Pertumbuhan mengandung pengertian pertambahan ukuran, dapat berupa volume, massa, tinggi, dan ukuran lainnya yang dapat dinyatakan dalam bilangan atau bentuk kuantitatif. Adapun perkembangan mengandung pengertian bertambah dewasanya suatu individu.</p>\n" +
                "<p>Makhluk hidup dikatakan dewasa jika alat-alat reproduksinya telah berfungsi. Tumbuhan akan berbunga dan hewan akan menghasilkan sel-sel kelamin. Ada pula yang mengartikan perkembangan sebagai perubahan akibat proses diferensiasi yang menyebabkan perbedaan struktur dan fungsi organ-organ makhluk hidup sehingga semakin kompleks. Dengan demikian, perkembangan merupakan perubahan kualitas suatu individu.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<ol>\n" +
                "<li><strong>Pertumbuhan</strong></li>\n" +
                "</ol>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Secara harfiah, pertumbuhan diartikan sebagai perubahan yang dapat diketahui atau ditentukan berdasarkan sejumlah ukuran atau kuantitasnya. Pertumbuhan meliputi bertambah besar dan bertambah banyaknya sel-sel pada jaringan. Proses yang terjadi pada pertumbuhan adalah suatu kegiatan yang irreversible (tidak dapat kembali ke bentuk semula).&nbsp;</p>\n" +
                "<p><br /> Akan tetapi, pada beberapa kasus, proses tersebut dapat reversible (terbalikkan) karena pada pertumbuhan terjadi pengurangan ukuran dan jumlah sel akibat kerusakkan sel atau dediferensiasi sel. Sebagai contoh, jika Anda akan memperbanyak tumbuhan melalui cara vegetatif, bagian manakah yang akan Anda pakai? Bunga, buah, ataukah batang? Pilihannya tentu akan jatuh pada batang.&nbsp;</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Walaupun semua organ tersebut memiliki aktivitas pembelahan sel, semuanya disusun oleh jenis sel yang berbeda. Bunga dan buah merupakan organ reproduksi yang disusun oleh sel-sel reproduktif atau embrionik, sedangkan cabang atau batang disusun oleh sel-sel tubuh atau somatik.&nbsp;</p>\n" +
                "<p><br /> Sel-sel tubuh (somatik) memiliki potensi untuk tumbuh kembali membentuk jaringan yang sama, sedangkan sel embrionik tidak. Dengan aktivitas perbanyakan sel tersebut, akan dihasilkan kembali sel-sel meristematis yang akan menjadi batang, akar, daun, dan bagian reproduktif.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Adapun sel embrionik akan mati karena tidak ada sokongan sel lainnya. Selama proses tumbuhnya akar, batang, ataupun daun pertumbuhan dapat dikuantifikasi dalam bentuk panjang akar, jumlah daun, tinggi tumbuhan, atau bahkan berat total tumbuhan. Berdasarkan gambaran tersebut, dapat ditarik suatu kesimpulan bahwa pertumbuhan merupakan perubahan kuantitatif dari ukuran sel, organ, atau keseluruhan organisme.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<ol start=\"2\">\n" +
                "<li><strong>Perkembangan</strong></li>\n" +
                "</ol>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Perkembangan makhluk hidup lebih tepat diartikan sebagai suatu perubahan kualitatif yang melibatkan perubahan struktur serta fungsi yang lebih kompleks. Seperti yang telah Anda ketahui, organ kulit pada manusia tumbuh bersamaan dengan bertambahnya ukuran tubuh. Akan tetapi, ketika mencapai kedewasaan, hanya pada bagian tertentu dari tubuh kita mulai bermunculan rambut tambahan.<br /> <br /> Selain itu, organ-organ tertentu mulai tumbuh membesar, seperti bagian dada pada perempuan dan jakun pada laki-laki. Mengapa semua itu hanya tumbuh pada masa tertentu saja, tidak bersamaan dengan pertumbuhan organ lainnya? Suatu hal yang patut kita pahami dalam perkembangan adalah adanya&nbsp;<em>diferensiasi sel</em>.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Diferensiasi dapat diartikan sebagai perubahan sel menjadi bentuk lainnya yang berbeda baik secara fungsi, ukuran, maupun bentuk. Contoh mudah mengenai diferensiasi dapat ditemukan pada pembentukan bunga. Amati dari mana bunga tersebut berasal. Apakah sama dengan awal mulanya tumbuh tunas? Mengapa pada bagian tersebut yang tumbuh justru bunga?<br /> <br /> Diferensiasi juga terjadi pada bagian tubuh manusia, yakni pada pembentukan sel-sel kelamin (gonad) ketika embriogenesis. Contoh lainnya pada proses pembentukan anak ayam dari embrio dalam telur. Pada proses diferensiasi, dapat terjadi dua hal penting, yakni perubahan struktural yang akan mengarah pada pembentukan organ, serta perubahan kimiawi yang dapat meningkatkan kemampuan sel.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Dapatkah Anda menghitung perkembangan yang terjadi, baik dalam jumlah maupun ukuran? Tentu akan sulit, karena semua proses tersebut terjadi secara kualitatif dan hanya dapat dibandingkan secara subjektif tanpa ukuran yang tepat. Proses perkembangan banyak berkaitan dengan faktor internal yang terjadi pada waktu yang tidak bersamaan. Oleh karena itu, perkembangan dapat didefinisikan sebagai suatu proses perubahan yang diikuti oleh pendewasaan dan kematangan sel, serta diiringi oleh spesialisasi fungsi sel.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<ol start=\"3\">\n" +
                "<li><strong>Macam-Macam Pertumbuhan dan Perkembangan</strong></li>\n" +
                "</ol>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Pertumbuhan pada tumbuhan ada yang berupa pertumbuhan primer, ada pula yang berupa pertumbuhan sekunder. Kedua pertumbuhan ini sebenarnya berasal dari jaringan yang sama, yakni meristem. Meristem merupakan suatu jaringan yang memiliki sifat aktif membelah. Pertumbuhan primer berasal dari meristem primer, sedangkan pertumbuhan sekunder berasal dari meristem sekunder. Adakah perbedaan lain di antara kedua macam pertumbuhan tersebut?</p>\n" +
                "<ol>\n" +
                "<li><strong><em> Pertumbuhan Primer</em></strong></li>\n" +
                "</ol>\n" +
                "<p>Pertumbuhan yang terjadi selama fase embrio sampai perkecambahan merupakan contoh pertumbuhan primer. Struktur embrio terdiri atas tunas embrionik yang akan membentuk batang dan daun, akar embrionik yang akan tumbuh menjadi akar, serta kotiledon yang berperan sebagai penyedia makanan selama belum tumbuh daun.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Jika biji berkecambah, struktur yang pertama muncul adalah radikula yang merupakan bakal akar primer. Radikula adalah bagian dari hipokotil dan merupakan struktur yang berasal dari akar embrionik. Pada bagian ujung atas, terdapat epikotil, yakni bakal batang yang berasal dari tunas embrionik.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Tahap awal pertumbuhan pada tumbuhan monokotil berbeda dengan dikotil. Pada monokotil, akan tumbuh koleoptil sebagai pelindung ujung bakal batang. Begitu koleoptil muncul di atas permukaan tanah, pucuk daun pertama akan muncul menerobos koleoptil. Biji masih tetap berada di dalam tanah dan memberi suplai makanan kepada kecambah yang sedang tumbuh. Perkecambahan seperti ini dinamakan perkecambahan&nbsp;<strong><em>hipogeal</em></strong></p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Bagaimanakah perkecambahan pada tumbuhan dikotil? Pada dikotil tidak muncul koleoptil. Dari dalam tanah, kotiledonnya akan muncul keatas permukaan tanah bersamaan dengan munculnya daun pertama. Kotiledon akan memberi makan bakal daun dan bakal akar sampai keduanya dapat mengadakan fotosintesis. Itulah sebabnya, lama-kelamaan kotiledon menjadi kecil dan kisut. Perkecambahan yang kotiledonnya terangkat ke permukaan tanah dinamakan perkecambahan&nbsp;<strong><em>epigeal</em></strong>.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Pada ujung pucuk dan ujung akar, terdapat jaringan yang bersifat meristematik. Jaringan meristem yang terletak di ujung akar menyebabkan pemanjangan akar. Pertambahan panjang akar pada jagung mencapai 1 cm per hari. Ujung akar akan menghasilkan tudung akar. Tudung akar akan menghasilkan lendir yang dapat mempermudah akar menembus tanah. Menurut Hopson (1990: 475), pada ujung akar terdapat tiga daerah pertumbuhan berturut-turut dari ujung ke pangkal, yakni daerah pembelahan, daerah pemanjangan, dan daerah diferensiasi.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Sel-sel di daerah pembelahan akan membelah secara mitosis sehingga selnya bertambah banyak. Daerah pemanjangan akan membentuk bakal epidermis ke arah luar. Pada daerah diferensiasi, sel-selnya akan berdiferensiasi membentuk komponen pembuluh angkut, epidermis, dan bulu-bulu akar.<br /> <br /> Ujung pucuk juga merupakan jaringan meristematik. Jaringan ini akan berdiferensiasi menjadi epidermis, floem, xilem, korteks, dan empulur. Meristem ini dilindungi oleh primordium daun. Letak primordium daun pada batang mengikuti pola berhadapan atau pola bergantian yang nantinya akan membentuk rangkaian daun sesuai dengan pola tersebut.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<ol>\n" +
                "<li><strong><em> Pertumbuhan Sekunder</em></strong></li>\n" +
                "</ol>\n" +
                "<p>Semakin tua, batang tumbuhan dikotil akan semakin membesar. Hal ini disebabkan adanya proses pertumbuhan sekunder. Pertumbuhan sekunder ini tidak terjadi pada tumbuhan monokotil. Bagian yang paling berperan dalam pertumbuhan sekunder ini adalah kambium dan kambium gabus atau felogen.<br /> <br /> Ke arah dalam, kambium akan membentuk pembuluh kayu (xilem), sedangkan ke arah luar kambium akan membentuk pembuluh tapis (floem). Kambium pada posisi seperti ini dinamakan kambium intravaskular. Sel-sel parenkim yang terdapat di antara pembuluh, lama-kelamaan berubah menjadi kambium. Kambium ini dinamakan kambium intervaskular.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Kedua macam kambium tersebut lama-kelamaan akan bersambungan. Posisi kambium yang semula terpisah-pisah, kemudian akan berbentuk lingkaran. Kedua macam kambium ini akan terus berkembang membentuk xilem sekunder dan floem sekunder sehingga batang menjadi semakin besar. &nbsp;Akibat semakin besarnya batang, diperlukan jalan untuk mengangkut makanan ke arah samping (lateral). Untuk keperluan tersebut, dibentuklah jari-jari empulur.&nbsp;</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Aktivitas kambium bergantung pada keadaan lingkungan. Pada musim kemarau, kambium tidak aktif. Walaupun aktif, kambium hanya akan membentuk sel-sel xilem berdiameter sempit. Ketika air berlimpah, kambium akan membentuk sel-sel xilem dengan diameter besar. Perbedaan ukuran diameter ini akan menyebabkan terbentuknya lingkaran-lingkaran pada penampang melintang batang. Lingkaran ini dikenal dengan lingkaran tahun, yang dapat digunakan untuk memperkirakan umur tumbuhan.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Sementara itu, kambium gabus atau felogen juga melakukan aktivitasnya. Felogen ini akan membentuk lapisan gabus. Ke arah dalam, felogen membentuk feloderm yang merupakan sel-sel hidup dan ke arah luar membentuk felem (jaringan gabus) yang merupakan sel-sel mati. Lapisan gabus perlu dibentuk karena fungsi epidermis sebagai pelindung tidak memadai lagi.</p>\n" +
                "<p>&nbsp;</p>\n" +
                "<p>Hal ini diakibatkan oleh pertumbuhan sekunder yang dilakukan kambium mendesak pertumbuhan ke arah luar. Hal tersebut mengakibatkan rusaknya epidermis sehingga kulit batang menjadi pecah-pecah. Adanya lapisan gabus mengakibatkan batang menjadi lebih terlindungi dari perubahan cuaca. Zat suberin pada sel-sel gabus dapat mencegah penguapan air dari batang. Agar pertukaran gas tetap berjalan lancar, di beberapa bagian dari permukaan batang terdapat lentisel.</p>";


        txJudul = (TextView) v.findViewById(R.id.tx_teori2_wizard_lesson_judul);
        txIsi = (TextView) v.findViewById(R.id.tx_teori2_wizard_lesson_isi);

        btnSebelumnya = (Button) v.findViewById(R.id.btn_teori2_sebelumnya);
        btnSelanjutnya = (Button) v.findViewById(R.id.btn_teori2_selanjutnya);

        halaman = (TextView) v.findViewById(R.id.tx_teori2_halaman);

        txIsi.setText(Html.fromHtml(isi));

        final ViewPager vp = (ViewPager) getActivity().findViewById(R.id.container2);

        btnSebelumnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() - 1);
            }
        });

        btnSelanjutnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vp.setCurrentItem(vp.getCurrentItem() + 1);
            }
        });
        return v;

    }

}
