package com.example.telc2.ulearnvd.data.model;

/**
 * Created by TEL-C on 8/11/17.
 */

public class SubLesson {

    private String nama;
    private String gambar;

    public SubLesson(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
