package com.example.telc2.ulearnvd.activity;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.telc2.ulearnvd.R;
import com.example.telc2.ulearnvd.fragment.KuisNo1Fragment;
import com.example.telc2.ulearnvd.fragment.KuisNo2Fragment;
import com.example.telc2.ulearnvd.fragment.KuisNo3Fragment;
import com.example.telc2.ulearnvd.fragment.KuisNo4Fragment;
import com.example.telc2.ulearnvd.fragment.KuisNo5Fragment;
import com.example.telc2.ulearnvd.fragment.Teori2WizardFragment;
import com.example.telc2.ulearnvd.fragment.TeoriWizardFragment;
import com.example.telc2.ulearnvd.fragment.Video2WizardFragment;
import com.example.telc2.ulearnvd.fragment.VideoWizardFragment;

import java.util.ArrayList;
import java.util.List;

public class TabbedKuisActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed_kuis);
        setTitle("Quis 1 Biologi");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container3);
        setupViewPager(mViewPager);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void setupViewPager(ViewPager viewPager) {
        TabbedKuisActivity.ViewPagerAdapter adapter = new TabbedKuisActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new KuisNo1Fragment(), "Kuis No 1");
        adapter.addFrag(new KuisNo2Fragment(), "Kuis No 2");
        adapter.addFrag(new KuisNo3Fragment(), "Kuis No 3");
        adapter.addFrag(new KuisNo4Fragment(), "Kuis No 4");
        adapter.addFrag(new KuisNo5Fragment(), "Kuis No 5");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
